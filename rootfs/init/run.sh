#!/bin/bash
#
#

set +e
set +u
set -x

ICINGA2_PARAMS=

finish() {
  rv=$?
  echo -e "\033[38;5;202m\033[1mexit with signal '${rv}'\033[0m"
  sleep 4s
  exit $rv
}

trap finish SIGINT SIGTERM INT TERM EXIT

. /etc/profile

# chmod o+rw -R /run/icinga2/


mkdir -p /var/lib/icinga2/certs/

# setup certificates
nc -l 6000 > /etc/icinga2/pki/ca.crt &&  cp /etc/icinga2/pki/ca.crt /var/lib/icinga2/certs/ca.crt
nc -l 7000 > /etc/icinga2/pki/${ICINGA2_AGENT}.csr  && cp /etc/icinga2/pki/${ICINGA2_AGENT}.csr /var/lib/icinga2/certs/${ICINGA2_AGENT}.csr
nc -l 8000 > /etc/icinga2/pki/${ICINGA2_AGENT}.key && cp /etc/icinga2/pki/${ICINGA2_AGENT}.key /var/lib/icinga2/certs//${ICINGA2_AGENT}.key
nc -l 10000 > /var/lib/icinga2/certs/${ICINGA2_AGENT}.crt
# fix premissions b/c root, and /run to be able to run icinga2
chown nagios:nagios -R /var/lib/icinga2/certs/
chmod o+rw -R /run/icinga2/


env
echo 'one'
# setup the master
cat > /etc/icinga2/zones.conf <<EOT

#### #### ####  MASTER  #### #### ####
object Endpoint "${ICINGA2_MASTER}" {
        #host = "${ICINGA2_MASTER}"
        #port = "5665"
}
object Zone "master" {
        endpoints = [ "${ICINGA2_MASTER}" ]
}
#### #### ####  MASTER  #### #### ####

#### #### ####  CLIENT  #### #### ####
# client ID = ${ICINGA2_AGENT}
object Endpoint "${ICINGA2_AGENT}" {
}
object Zone "${ICINGA2_AGENT}" {
        endpoints = [ "${ICINGA2_AGENT}" ]
        parent = "master"
}
#### #### ####  CLIENT  #### #### ####

object Zone "global-templates" {
        global = true
}

object Zone "director-global" {
        global = true
}

EOT

echo 'two'

# Enable API / passive mode
cat > /etc/icinga2/features-enabled/api.conf <<EOT
object ApiListener "api" {
  accept_config = true
  accept_commands = true
  bind_port  = 5775
}

EOT

echo 'three'
# if [[ -z ${DEBUG+x} ]]; then echo "DEBUG is unset"; else echo "DEBUG is set to '$DEBUG'"; fi

if [[ ! -z ${DEBUG+x} ]]
then
  env | grep DEBUG

  if [[ ! "${DEBUG}" = "true" ]] && ( [[ "${DEBUG}" = "true" ]] || [[ ${DEBUG} -eq 1 ]] )
  then
    set -x
    ICINGA2_PARAMS="--log-level debug"
  fi
fi

# [[ -f /etc/environment ]] && . /etc/environment

. /init/output.sh
. /usr/bin/vercomp
. /init/environment.sh
. /init/runtime/service_handler.sh

# -------------------------------------------------------------------------------------------------

# side channel to inject some wild-style customized scripts
# THIS CAN BREAK THE COMPLETE ICINGA2 CONFIGURATION!
#
custom_scripts() {

  if [[ -d /init/custom.d ]]
  then
    for f in /init/custom.d/*
    do
      case "$f" in
        *.sh)
          log_WARN "------------------------------------------------------"
          log_WARN "RUN SCRIPT: ${f}"
          log_WARN "YOU SHOULD KNOW WHAT YOU'RE DOING."
          log_WARN "THIS CAN BREAK THE COMPLETE ICINGA2 CONFIGURATION!"
          nohup "${f}" > /dev/stdout 2>&1 &
          log_WARN "------------------------------------------------------"
          ;;
        *)
          log_warn "ignoring file ${f}"
          ;;
      esac
      echo
    done
  fi
}


run() {

  log_info "prepare system"

  . /init/common.sh

  prepare

  validate_certservice_environment

  version_of_icinga_master

  [[ "${ICINGA2_TYPE}" = "Master" ]] && . /init/database/mysql.sh

  . /init/configure_icinga.sh
  . /init/api_user.sh

  if [[ "${ICINGA2_TYPE}" = "Master" ]]
  then
    . /init/graphite_setup.sh
    . /init/configure_ssmtp.sh
  fi

  correct_rights

  log_info "---------------------------------------------------"
  log_info " Icinga ${ICINGA2_TYPE} Version ${ICINGA2_VERSION} - build: ${BUILD_DATE}"
  log_info "---------------------------------------------------"

  custom_scripts

  log_info "start init process ..."

  if [[ "${ICINGA2_TYPE}" = "Master" ]]
  then
    # backup the generated zones
    #
    nohup /init/runtime/inotify.sh > /dev/stdout 2>&1 &

    # env | grep ICINGA | sort
    if [[ -f /usr/local/icinga2-cert-service/bin/icinga2-cert-service.rb ]]
    then
      nohup /usr/local/icinga2-cert-service/bin/icinga2-cert-service.rb > /dev/stdout 2>&1 &
    fi
  else
    :
    # nohup /init/runtime/ca_validator.sh > /dev/stdout 2>&1 &

    #if [[ ! -e /tmp/final ]]
    #then
    #  # nohup /init/runtime/zone_watcher.sh > /dev/stdout 2>&1 &
    #fi
  fi

  /usr/sbin/icinga2 \
    daemon \
    ${ICINGA2_PARAMS}
}

run

# EOF
